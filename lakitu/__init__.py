import version
__version__ = "{}.{}.{}".format(version.LAKITU_MAJOR_VERSION, version.LAKITU_MINOR_VERSION, version.LAKITU_PATCH_VERSION)
