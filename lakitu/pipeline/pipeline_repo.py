import json
import logging
from collections import defaultdict, namedtuple
from os import path
from urllib2 import urlopen

import yaml

logger = logging.getLogger(__name__)

DEFAULT_REPO = "https://s3.amazonaws.com/atkeller.lakitu.public/pipelines/pipelines.yaml"


class PipelineSourceEntry(object):
    def __init__(self, source_type, pipeline_name, version):
        self.source_type = source_type
        self.pipeline_name = pipeline_name
        self.version = str(version)


class CondaPipelineEntry(PipelineSourceEntry):
    def __init__(self, channel, pipeline_name, version):
        super(CondaPipelineEntry, self).__init__(source_type="conda",
                                                 pipeline_name=pipeline_name,
                                                 version=version)
        self.channel = channel


PipelineKey = namedtuple("PipelineKey", ["pipeline_name", "version"])


class PipelineRepository(object):
    def __init__(self, repo_index_yml=None):
        """Pull down the yaml file for the repository. Can be URL or file"""
        if repo_index_yml is None:
            self._repo_source = DEFAULT_REPO
        else:
            self._repo_source = repo_index_yml
        self._yaml_data = None  # Cache the yaml data
        self.pipelines = self._process_repo_index(self._repo_source)

    def _process_repo_index(self, repo_yml):
        """
        Read the repo_yml file and populate the internal index of available pipelines
        :param repo_yml: either a URL pointing to a yaml file or a local file path
        :return:
        """
        pipelines = dict()  # dict of (pipeline name ,version) -> pipelinentry
        if path.isfile(repo_yml):
            f = open(repo_yml)
        else:
            f = urlopen(repo_yml)
        self._yaml_data = yaml.safe_load(f)
        for package_source in self._yaml_data['PackageSources']:
            for pipeline in self.pipelines_from_package_source(package_source):
                key = PipelineKey(pipeline.pipeline_name, pipeline.version)
                if key in pipelines:
                    logger.warn("Parsing repository {} found multiple entries for "
                                 "pipeline {} {}".format(repo_yml, pipeline.pipeline_name, pipeline.pipeline_version))
                else:
                    pipelines[key] = pipeline
        return pipelines

    @staticmethod
    def pipelines_from_package_source(package_source):
        """
        Parse package source, return listing of pipeline objects
        :param package_source: parsed yaml entry for package source
        :return: a generator that yields pipeline objects
        """
        package_source_type = package_source['Type']
        if package_source_type == 'conda_repository':
            return PipelineRepository.pipelines_from_conda_repository(package_source)
        else:
            raise Exception("Unknown package source: {}".format(package_source_type))

    @staticmethod
    def pipelines_from_conda_repository(package_source):
        """
        Parse a conda repository yaml entry yielding pipeline objects
        :param package_source: parsed yaml entry for package source with Type conda_repository
        :return: yield pipeline objects
        """
        channel_name = package_source['ChannelName']
        for pipeline in package_source['Pipelines']:
            pipeline_name = pipeline['PipelineName']
            for version in pipeline['PipelineVersions']:
                yield CondaPipelineEntry(channel=channel_name,
                                         pipeline_name=pipeline_name,
                                         version=str(version))

    def get_yaml(self):
        return self._yaml_data

    def get_json(self):
        return json.dumps(self._yaml_data)

    def list_pipelines(self):
        """
        :return: a sorted list of available pipelines
        """
        return sorted(list(set((k.pipeline_name for k in self.pipelines.keys()))))

    def list_pipelines_and_versions(self):
        """
        :return: a dictionary with keys being pipeline names and values being list of versions
        """
        pipes_and_versions = defaultdict(list)
        for pipeline_name, version in self.pipelines.keys():
            pipes_and_versions[pipeline_name].append(version)
        for key in pipes_and_versions:
            pipes_and_versions[key] = sorted(pipes_and_versions[key])
        return dict(pipes_and_versions)

    def get_pipeline(self, pipeline_name, version):
        key = PipelineKey(pipeline_name=pipeline_name,
                          version=version)
        try:
            return self.pipelines[key]
        except KeyError:
            logger.error("Tried to access a pipeline / version that does not exist"
                          " in the repository: {} / {}".format(*key))
            raise
