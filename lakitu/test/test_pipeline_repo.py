import unittest
import os
from lakitu.pipeline.pipeline_repo import PipelineRepository


class TestPipelineRepo(unittest.TestCase):
    expected_pipelines = sorted(['EncyclopediaDataQuery', 'TestDummy'])
    expected_pipelines_and_versions = {'EncyclopediaDataQuery': ['0.01', '0.02', 'super_special_edition'],
                                       'TestDummy': ['0.01']}

    def setUp(self):
        self._data_dir = os.path.dirname(os.path.realpath(__file__))

    def test_local_parse(self):
        pr = PipelineRepository(os.path.join(self._data_dir, 'test_pipeline_repo.yaml'))
        self.assertEqual(pr.list_pipelines(), self.expected_pipelines)
        self.assertEqual(pr.list_pipelines_and_versions(), self.expected_pipelines_and_versions)

    def test_s3_parse(self):
        pr = PipelineRepository("https://s3.amazonaws.com/jegertso.lakitu/pipelines/test/test_pipeline_repo.yaml")
        self.assertEqual(pr.list_pipelines(), self.expected_pipelines)
        self.assertEqual(pr.list_pipelines_and_versions(), self.expected_pipelines_and_versions)

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestPipelineRepo)
    unittest.TextTestRunner(verbosity=3).run(suite)
