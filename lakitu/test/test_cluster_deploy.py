import unittest
from argparse import ArgumentTypeError

from botocore.exceptions import EndpointConnectionError

from lakitu.cli.cluster_deploy import cname_prefix_arg
from lakitu.cli.cluster_deploy import get_optimized_ecs_ami_for_region


class TestGetECSOptimizedAmi(unittest.TestCase):

    # AWS Regions with associated ECS-optimized AMIs as of March 2018
    aws_regions = [
        'us-east-2',
        'us-east-1',
        'us-west-2',
        'us-west-1',
        'eu-west-3',
        'eu-west-2',
        'eu-west-1',
        'eu-central-1',
        'ap-northeast-2',
        'ap-northeast-1',
        'ap-southeast-2',
        'ap-southeast-1',
        'ca-central-1',
        'ap-south-1',
        'sa-east-1',
    ]

    def test_get_optimized_ecs_for_region__get_linux_and_windows_amis(self):
        ami_regex = r'^ami-([a-zA-Z0-9]{8}|[a-zA-Z0-9]{17})$'  # AMI Ids can either be 8 or 17 characters
        good_ami_ids = ['ami-12345678', 'ami-a1s2S354', 'ami-a1491ad2', 'ami-0a0d2004b44b9287c']
        bad_ami_ids = ['asdvklj', '', 'blahami-12345678', 'ami-123', 'ami-', 'ami-$2345678', 'ami-123456789']

        for ami_id in good_ami_ids:
            self.assertRegexpMatches(ami_id, ami_regex)

        for ami_id in bad_ami_ids:
            self.assertNotRegexpMatches(ami_id, ami_regex)

        for region in self.aws_regions:
            for os in ['linux', 'windows']:
                try:
                    ami_id = get_optimized_ecs_ami_for_region(aws_region=region, os=os)
                    self.assertRegexpMatches(ami_id, ami_regex)
                except:
                    self.fail('Failed to get latest ECS AMIs')

    def test_get_optimized_ecs_for_region__incorrect_region(self):
        kwargs = {'aws_region': 'us-east-3', 'os': 'linux'}
        msg_regex = r"^Could not connect to the endpoint URL: \"https://ssm.us-east-3.amazonaws.com/\"$"
        self.assertRaisesRegexp(EndpointConnectionError, msg_regex, get_optimized_ecs_ami_for_region, **kwargs)

    def test_get_optimized_ecs_for_region__incorrect_os(self):
        kwargs = {'aws_region': 'us-east-2', 'os': 'mac'}
        msg_regex = r'^mac is not a recognized OS\. Must be either linux or windows\.$'
        self.assertRaisesRegexp(ValueError, msg_regex, get_optimized_ecs_ami_for_region, **kwargs)


class TestValidateArgs(unittest.TestCase):

    def test_api_url_prefix(self):
        api_url_prefix_test_cases = {
            'valid': {
                'lakitu-api',
                'abcd',
                'ab-c-d',
                'a--b',
                'ABcD'
            },
            'invalid': {
                '',
                '-',
                '-ab',
                'ab-',
                'a.b',
                'aBc.de',
            }
        }
        for value in api_url_prefix_test_cases['valid']:
            self.assertEqual(value, cname_prefix_arg(value))

        for value in api_url_prefix_test_cases['invalid']:
            self.assertRaises(ArgumentTypeError, cname_prefix_arg, value)


if __name__ == '__main__':
    pass
    #loader = unittest.TestLoader()
    #suite = unittest.TestLoader().loadTestsFromTestCase(TestGetECSOptimizedAmi)
    #unittest.TextTestRunner(verbosity=3).run(suite)
