from sqlalchemy import create_engine
from sqlalchemy import MetaData
import time

engine = create_engine


class RunStatusManager(object):

    def __init__(self):
        self.engine = create_engine('postgresql://postgres:postgres@localhost:5432/luigi-task-hist')


    def get_main_task_id(self, previous=None):
        result = self.get_most_recent_main_task()
        if(previous != None):
            while result == previous:
                time.sleep(1)
                result = self.get_most_recent_main_task()
        
        return result

    def get_latest_run_status(self, run_id):
        task_id = self.engine.execute('select id from tasks where task_id like \'' + run_id + '\'').fetchone()[0]
        return self.engine.execute('select event_name from task_events where task_id = ' + str(task_id) + 'order by id desc fetch first 1 rows only').fetchone()[0]


    def get_most_recent_main_task(self):
        return self.engine.execute('select task_id from tasks where name like \'MainTask\' order by id desc fetch first 1 rows only;').fetchone()[0]