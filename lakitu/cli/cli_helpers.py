import os
from functools import wraps


def find_config_location():
    """
    Search for lakitu.cfg in environment variable LAKITU_CONF
    current directory
    home directory/.lakitu
    home directory
    """
    home_dir = os.path.expanduser("~")

    # try to read the path from teh environment variable LAKITU_CONF_PATH
    lcp = os.environ.get("LAKITU_CONF_PATH")
    if lcp is not None:
        try:
            with open(lcp):
                return lcp
        except IOError:
            pass

    for loc in os.curdir, os.path.join(home_dir, ".lakitu"), os.path.expanduser("~"):
        try:
            conf_p = os.path.join(loc, "lakitu.cfg")
            with open(conf_p):
                return conf_p
        except IOError:
            pass
    return None


def suggest_install_location():
    return os.path.join(os.path.expanduser('~'), '.lakitu')


def suggest_config_location():
    return os.path.join(suggest_install_location(), 'lakitu.cfg')


def get_user_input(message):
    output = raw_input(message + ': ')
    print ""
    return output


def get_user_input_with_default(message, default):
    response = get_user_input(message + " [{}]".format(default))
    if response.strip() == "":
        return default
    else:
        return response


def get_user_yn_input(message):
    s_input = get_user_input(message + ' [y/n]').strip().upper()
    if s_input in ('Y', 'YES'):
        return True
    elif s_input in ('N', 'NO'):
        return False
    else:
        print "Please enter Y or N"
        return get_user_yn_input(message)


def add_config_argument(parser):
    parser.add_argument('--config-file', type=os.path.abspath, help="Set the location of the Lakitu config file",
                        default=find_config_location())


def needsconfig(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if args[0].config_file is None:
            raise Exception("No configuration file found, set "
                            "the location of the config file with the --config-file flag. "
                            "Or create a new one by running lakitu config")
        return f(*args, **kwargs)
    return decorated_function
