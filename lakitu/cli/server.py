import ConfigParser

import cli_helpers as ch
import lakitu.server


def configure_parser(parser):
    ch.add_config_argument(parser)
    parser.add_argument("--host", default="localhost")
    parser.add_argument("--port", default=5002)
    parser.add_argument("--debug",
                        help="Allow server to return helpful debugging output. Not for use in production.",
                        action="store_true")


@ch.needsconfig
def run(args):
    lakitu.server.run(address=args.host, api_port=args.port, debug=args.debug)
