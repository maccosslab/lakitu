"""
Lakitu configuration interface for cli
"""

import ConfigParser
import os
import shutil
from StringIO import StringIO
from lakitu.pipeline import pipeline_repo

import cli_helpers as ch


def configure_parser(parser):
    """Parser has no additional arguments"""
    pass


def run(args=None):
    print "Welcome to Lakitu configuration."
    existing_config = ch.find_config_location()
    if existing_config is not None:
        if not ch.get_user_yn_input("An existing Lakitu configuration file was found at {}. "
                                    "It may be overwritten. Continue?".format(existing_config)):
            return

    # create an install directory
    install_loc = ch.get_user_input_with_default("Specify a location for the lakitu installation. "
                                                 "It is recommended to use the default here if possible. "
                                                 "This way the --configure-file flag will not need to be used "
                                                 "to run pipelines. Press enter to select the default.",
                                                 ch.suggest_install_location())

    install_loc = os.path.abspath(install_loc)

    if not os.path.exists(install_loc):
        print "Creating directory {}".format(install_loc)
        os.makedirs(install_loc)

    envs_loc = ch.get_user_input_with_default("Specify a location for Lakitu pipeline environments to be "
                                              "installed. Press enter to accept the default.",
                                              os.path.join(install_loc, 'envs'))
    envs_loc = os.path.abspath(envs_loc)
    if not os.path.exists(envs_loc):
        print "Creating directory {}".format(envs_loc)
        os.makedirs(envs_loc)

    lakitu_cfg_loc = ch.get_user_input_with_default("Specify a location to write the lakitu configuration file.",
                                                    os.path.join(install_loc, 'lakitu.cfg'))

    lakitu_cfg_loc = os.path.abspath(lakitu_cfg_loc)

    pipe_index = ch.get_user_input_with_default("Specify a pipeline index",
                                                pipeline_repo.DEFAULT_REPO)

    aws_cred_default = os.path.join(os.path.expanduser('~'), '.aws', 'credentials')
    aws_cred_file = None
    if os.path.exists(aws_cred_default):
        if ch.get_user_yn_input("An existing AWS credentials file was found at {} . Would you like to use these "
                                "credentials for logging in to AWS?".format(aws_cred_default)):
            aws_cred_file = aws_cred_default

    if aws_cred_file is None:
        cred_path = ch.get_user_input_with_default("No AWS credentials file was found, please specify the location of "
                                                   "your existing credentials file or press enter to configure a new "
                                                   "one.", "")

        while cred_path != "" and not os.path.exists(cred_path):
            cred_path = ch.get_user_input_with_default("The provided credentials path {} does not exist. "
                                                       "Please input a valid path or press enter to create a new "
                                                       "credentials file".format(cred_path), "")
        if cred_path != "":
            suggested_cred_path = os.path.join(install_loc, 'credentials')
            copy_creds = ch.get_user_yn_input("Lakitu can reference the credentials you passed ({}), or copy them to "
                                              "the lakitu install directory ({}). It is recommended to copy the "
                                              "credentials to your lakitu installation directory, if that location "
                                              "is secure. Would you like to copy "
                                              "the credentials? ".format(cred_path, suggested_cred_path))
            if copy_creds:
                try:
                    shutil.copy(cred_path, suggested_cred_path)
                except:
                    print "Error copying credentials, using reference to {}".format(cred_path)
                    aws_cred_file = cred_path
                else:
                    aws_cred_file = suggested_cred_path

    if aws_cred_file is None:
        aws_cred_file = aws_cred_default
        make_new_creds = False
        if ch.get_user_yn_input("An AWS credentials file is required to run lakitu pipelines. You will need your AWS "
                                "access key ID and Secret access key handy to create one. If you do not create the "
                                "credentials file now a reference to a file located at {} will be added to the Lakitu"
                                " config and you will need to put a credentials file there. "
                                "(see https://docs.aws.amazon.com/cli/latest/userguide/cli-config-files.html). "
                                "Create a credentials file now?".format(aws_cred_file)):
            make_new_creds = True
        if make_new_creds and ch.get_user_yn_input("A new credentials file will be created at {}, "
                                                   "continue?".format(aws_cred_default)):
                if os.path.exists(aws_cred_default):
                    if ch.get_user_yn_input("A credentials file already exists at {}, it will be overwritten, "
                                            "continue?".format(aws_cred_default)):
                        make_new_creds = True
                    else:
                        make_new_creds = False
                else:
                    make_new_creds = True
        if make_new_creds:
            access_key_id = ch.get_user_input("Enter your AWS key ID")
            secret_access_key = ch.get_user_input("Enter you secret access key ID")
            aws_config = ConfigParser.RawConfigParser()
            aws_config.readfp(StringIO('[default]'))
            aws_config.set('default', 'aws_access_key_id', access_key_id)
            aws_config.set('default', 'aws_secret_access_key', secret_access_key)
            try:
                if not os.path.exists(os.path.dirname(aws_cred_default)):
                    os.makedirs(os.path.dirname(aws_cred_default))
                with open(aws_cred_default, 'wb') as configfile:
                    aws_config.write(configfile)
            except:
                print "Error writing AWS credentials file"
            else:
                print "Successfully wrote AWS credentials to {}".format(aws_cred_default)

    aws_cred_file = os.path.abspath(aws_cred_file)
    luigi_cfg_loc = os.path.join(install_loc, 'luigi.cfg')

    cluster_cfg_loc = os.path.join(install_loc, 'clusters.cfg')

    # write the lakitu configuration file
    lakitu_cfg = ConfigParser.RawConfigParser()

    lakitu_cfg.add_section('conda')
    lakitu_cfg.set('conda', 'local_pipeline_directory', envs_loc)

    lakitu_cfg.add_section('package_repository')
    lakitu_cfg.set('package_repository', 'repository_index', pipe_index)

    lakitu_cfg.add_section('luigi')
    lakitu_cfg.set('luigi', 'config_location', luigi_cfg_loc)

    lakitu_cfg.add_section('aws')
    lakitu_cfg.set('aws', 'credentials_file', aws_cred_file)
    lakitu_cfg.set('aws', 'clusters_cfg', cluster_cfg_loc)

    try:
        if not os.path.exists(os.path.dirname(lakitu_cfg_loc)):
            os.makedirs(os.path.dirname(lakitu_cfg_loc))
        with open(lakitu_cfg_loc, 'wb') as configfile:
            lakitu_cfg.write(configfile)
    except:
        print "Error writing lakitu configuration file {}".format(lakitu_cfg_loc)
    else:
        print "Successfully wrote lakitu configuration file {}".format(lakitu_cfg_loc)

    # write the luigi config file
    luigi_cfg = ConfigParser.RawConfigParser()
    luigi_cfg.add_section('core')
    luigi_cfg.set('core', 'default-scheduler-host', 'localhost')
    luigi_cfg.set('core', 'default-scheduler-port', '8082')
    luigi_cfg.set('core', 'max_reschedules', '5')
    luigi_cfg.set('core', 'log_level', 'INFO')

    try:
        with open(luigi_cfg_loc, 'wb') as configfile:
            luigi_cfg.write(configfile)
    except:
        print "Error writing luigi configuration file {}".format(luigi_cfg_loc)
    else:
        print "Successfully wrote luigi configuration file {}".format(luigi_cfg_loc)
