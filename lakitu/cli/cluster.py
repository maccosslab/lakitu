"""
Lakitu cluster management interface for cli
"""
import ConfigParser
import argparse
import os
import shutil
import sys

import cli_helpers as ch
import cluster_deploy


def configure_parser(parser):
    subparsers = parser.add_subparsers(title='subcommands',
                                       description="cluster subcommands",
                                       help="additional help",
                                       dest='subcommand')

    # deploy
    parser_deploy = subparsers.add_parser('deploy', help="Deploy a Lakitu cluster on the cloud.",
                                          formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    cluster_deploy.configure_parser(parser_deploy)
    parser_deploy.set_defaults(func=cluster_deploy.deploy)

    # import
    parser_import = subparsers.add_parser('import', help="Import Lakitu cluster settings from a cluster config file.",
                                          formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    configure_parser_import(parser_import)
    parser_import.set_defaults(func=run_import)

    # export
    parser_export = subparsers.add_parser('export', help="Export current  Lakitu cluster settings",
                                          formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    configure_parser_export(parser_export)
    parser_export.set_defaults(func=run_export)


def get_cluster_cfg_loc(lakitu_cfg_loc):
    lakitu_cfg = ConfigParser.RawConfigParser()
    lakitu_cfg.read(lakitu_cfg_loc)
    return lakitu_cfg.get('aws', 'clusters_cfg')


def configure_parser_import(parser):
    ch.add_config_argument(parser)
    parser.add_argument('clusterdef', help="Lakitu cluster definition file to install",
                        type=os.path.abspath)


@ch.needsconfig
def run_import(args):
    if not os.path.exists(args.clusterdef):
        print "Could not find cluster definition file {} ".format(args.clusterdef)
        return
    cluster_file_loc = get_cluster_cfg_loc(args.config_file)
    try:
        shutil.copy(args.clusterdef, cluster_file_loc)
    except:
        print "Error importing cluster defintion file"
        raise
    else:
        print "Successfully imported cluster settings. Jobs will now be submitted to this cluster"


def configure_parser_export(parser):
    ch.add_config_argument(parser)


@ch.needsconfig
def run_export(args):
    cluster_file_loc = get_cluster_cfg_loc(args.config_file)
    with open(cluster_file_loc) as fin:
        sys.stdout.write(fin.read())
