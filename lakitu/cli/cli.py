"""cli.py
A command line interface for lakitu
"""

import argparse
import logging
import sys

import cluster
import configure_lakitu
import list_pipelines
import run_pipelines
import server


def get_parser():
    # configuration location with default set to get_config_location
    # top level parser
    parser = argparse.ArgumentParser(
        description="Command line interface for the Lakitu AWS pipeline.",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    subparsers = parser.add_subparsers(title='subcommands',
                                       description='valid subcommands',
                                       help='additional help',
                                       dest='subcommand')

    # sub command parsers

    # create the parser for list command -- list pipelines in repository
    parser_list = subparsers.add_parser('list', help="List pipelines in repository",
                                        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    list_pipelines.configure_parser(parser_list)
    parser_list.set_defaults(func=list_pipelines.run)

    # parser for the run command
    parser_run = subparsers.add_parser('run', help="Run a pipeline from the repository",
                                       formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    run_pipelines.configure_parser(parser_run)
    parser_run.set_defaults(func=run_pipelines.run)

    # parser for the config command
    parser_config = subparsers.add_parser('config', help="Configure a Lakitu installation",
                                          formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    configure_lakitu.configure_parser(parser_config)
    parser_config.set_defaults(func=configure_lakitu.run)

    parser_cluster = subparsers.add_parser('cluster', help="Lakitu cluster functions")
    cluster.configure_parser(parser_cluster)

    parser_server = subparsers.add_parser('server', help="Start daemon that provides REST API")
    server.configure_parser(parser_server)
    parser_server.set_defaults(func=server.run)

    return parser


def lakitu():
    DEFAULT_LOGGING = {
        'version': 1,
        'disable_existing_loggers': False,
        'formatters': {
            'standard': {
                'format': '[%(levelname)-8s %(asctime)s: %(name)s.%(funcName)s] %(message)s'
            },
        },
        'handlers': {
            'default': {
                'level': 'INFO',
                'formatter': 'standard',
                'class': 'logging.StreamHandler',
            },
        },
        'loggers': {
            '': {
                'handlers': ['default'],
                'level': 'INFO',
                'propagate': True
            },
        }
    }
    import logging.config
    logging.config.dictConfig(DEFAULT_LOGGING)

    parser = get_parser()
    args = parser.parse_args()
    # parse the config
    args.func(args)


if __name__ == '__main__':
    lakitu()
