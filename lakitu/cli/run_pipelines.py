import ConfigParser
import argparse
import logging
import os
import platform
import subprocess

from lakituapi.pipeline_executor import PipelineExecutor

import cli_helpers as ch
from lakitu.pipeline.pipeline_repo import PipelineRepository

logger = logging.getLogger(__name__)


def configure_parser(parser):
    ch.add_config_argument(parser)
    parser.add_argument('pipeline_name', help="Name of the pipeline")
    parser.add_argument('pipeline_version', help="Pipeline version")
    parser.add_argument('run_args', nargs=argparse.REMAINDER,
                        help="Arguments specific to the pipeline to be run")


@ch.needsconfig
def run(args):
    config = ConfigParser.RawConfigParser()
    config.read(args.config_file)
    config.read(config.get('aws', 'clusters_cfg'))

    repo_location = config.get('package_repository', 'repository_index')
    if repo_location == "":
        repo_location = None
    pr = PipelineRepository(repo_index_yml=repo_location)
    pipeline = pr.get_pipeline(pipeline_name=args.pipeline_name,
                               version=args.pipeline_version)
    if pipeline.source_type == "conda":
        run_conda_pipeline(pipeline, config, args.run_args)
    else:
        logger.error("Unknown pipeline source type: {}".format(pipeline.source_type))


def run_conda_pipeline(pipeline, config, pipeline_args=list()):
    pipeline_env_name = "{}.{}".format(pipeline.pipeline_name, pipeline.version)
    # check if lakitu conda envs path exists, create if need to
    conda_envs_dir = os.path.expanduser(config.get('conda', 'local_pipeline_directory'))
    if not os.path.exists(conda_envs_dir):
        try:
            os.makedirs(conda_envs_dir)
        except OSError:
            logger.error("Could not create local conda environments directory {}".format(conda_envs_dir))
            raise

    # check if pipeline directory exists
    pipeline_env_dir = os.path.join(conda_envs_dir, pipeline_env_name)
    if not os.path.exists(pipeline_env_dir):
        # make the environment
        # pull the pipeline from the repository
        # configure channels and paths
        conda_call = ["conda",
                      "create",
                      "-y",
                      "-p", pipeline_env_dir,
                      "-c", pipeline.channel,
                      "-c", "atkeller",  # needed to get lakituapi
                      "-c", "defaults",
                      "-c", "system",
                      "python=2.7",
                      "{}={}".format(pipeline.pipeline_name, pipeline.version)]
        ret_code = subprocess.call(conda_call)
        if ret_code != 0:
            logger.warn("Creation of conda environment returned non-zero exit status")

    # set an environ
    if platform.system().upper() == 'WINDOWS':
        python_binary = os.path.join(pipeline_env_dir, "python")
    else:
        python_binary = os.path.join(pipeline_env_dir, "bin", "python")
    interface_module = pipeline.pipeline_name + '.interface'

    # start luigid if not already running
    FNULL = open(os.devnull, 'w')
    # if luigid is running, a duplicate instance will not be created
    os.environ["LUIGI_CONFIG_PATH"] = os.path.expanduser(config.get('luigi', 'config_location'))
    p = subprocess.Popen('luigid', stdout=FNULL, stderr=FNULL)

    pipeline_call = [python_binary, "-m", "luigi", "--workers", "1000", "--module", interface_module, "MainTask"]
    pipeline_call.extend(pipeline_args)
    my_env = PipelineExecutor(config=config).execution_environment
    subprocess.call(pipeline_call, env=my_env)

    FNULL.close()
