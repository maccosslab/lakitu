import ConfigParser
import os
import sys

import cli_helpers as ch
from lakitu.pipeline.pipeline_repo import PipelineRepository


def configure_parser(parser):
    ch.add_config_argument(parser)


@ch.needsconfig
def run(args, output_stream=sys.stdout):
    config = ConfigParser.RawConfigParser()
    config.read(args.config_file)

    repo_location = config.get('package_repository', 'repository_index')
    if repo_location == "":
        repo_location = None
    pr = PipelineRepository(repo_index_yml=repo_location)
    pipelines = pr.list_pipelines_and_versions()
    for pipeline in sorted(pipelines.keys()):
        output_stream.write("--------------------------------" + os.linesep)
        output_stream.write("Pipeline: {}".format(pipeline) + os.linesep)
        output_stream.write("Versions: ")
        for i, version in enumerate(sorted(pipelines[pipeline])):
            if i == 0:
                output_stream.write(version + os.linesep)
            else:
                output_stream.write("          {}".format(version) + os.linesep)
