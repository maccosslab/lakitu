#!/usr/bin/env python

"""
Deploy a Lakitu cluster on the cloud
"""
import ConfigParser
import contextlib
import getpass
import json
import logging
import os
import posixpath
import random
import re
import shutil
import string
import tempfile
import time
from argparse import ArgumentTypeError
from collections import namedtuple
from tempfile import NamedTemporaryFile
from zipfile import ZipFile

import boto3
import botocore.session
import requests
from botocore.exceptions import WaiterError
from lakituapi.pipeline_executor import PipelineExecutorConfig

import cli_helpers as ch

logger = logging.getLogger(__name__)


class ConfigLogger(object):
    def __init__(self, log):
        self.__log = log

    def __call__(self, config):
        self.__log.info("Config:")
        config.write(self)

    def write(self, data):
        # stripping the data makes the output nicer and avoids empty lines
        line = data.strip()
        self.__log.info(line)


def get_optimized_ecs_ami_for_region(aws_region, os):
    """
    Amazon ECS-Optimized AMIs
    The latest image ids are listed on this page:
    https://docs.aws.amazon.com/AmazonECS/latest/developerguide/ecs-optimized_AMI.html
    We use the method from this page for retrieving the latest images through Amazon's API:
    https://docs.aws.amazon.com/AmazonECS/latest/developerguide/retrieve-ecs-optimized_AMI.html
    :param aws_region: any AWS region with ECS support, e.g. 'us-east-1'
    :param os: 'linux' or 'windows'
    :return: the ECS-optimized AMI image ID for the specified region and os
    """
    os_to_parameter = {
        'linux': '/aws/service/ecs/optimized-ami/amazon-linux/recommended',
        'windows': '/aws/service/ecs/optimized-ami/windows_server/2016/english/full/recommended',
    }

    try:
        parameter_name = os_to_parameter[os]
    except KeyError:
        raise ValueError('%s is not a recognized OS. Must be either linux or windows.' % os)

    session = botocore.session.Session()
    client = session.create_client(service_name='ssm', region_name=aws_region)
    ami_id = json.loads(client.get_parameter(Name=parameter_name)['Parameter']['Value'])['image_id']
    return ami_id


instance_user_data = """Content-Type: multipart/mixed; boundary="==BOUNDARY=="
MIME-Version: 1.0

--==BOUNDARY==
Content-Type: text/cloud-boothook; charset="us-ascii"

# Set Docker daemon options
cloud-init-per once docker_options echo 'OPTIONS="${{OPTIONS}} --storage-opt dm.basesize={LAKITU_STORAGE_SIZE}G"' >> /etc/sysconfig/docker

--==BOUNDARY==
Content-Type: text/x-shellscript; charset="us-ascii"

#!/bin/bash
# Install the SSM agent for the AWS Systems Manager (see https://docs.aws.amazon.com/systems-manager/latest/userguide/sysman-install-startup-linux.html)
cd /tmp
sudo yum install -y https://s3.amazonaws.com/ec2-downloads-windows/SSMAgent/latest/linux_amd64/amazon-ssm-agent.rpm
sudo start amazon-ssm-agent

--==BOUNDARY==--
"""


def cname_prefix_arg(value):
    """
    Validates the --api-url-prefix argument
    :param string value: automatically passed by the ArgumentParser object
    :return: Returns the value back to the ArgumentParser object
    """
    if not value or value.startswith('-') or value.endswith('-') or not re.match('^[a-zA-Z0-9\-]+$', value):
        raise ArgumentTypeError('Member must contain only letters, digits, and the dash character and may not start'
                                ' or end with a dash')
    return value


def configure_parser(parser):
    ch.add_config_argument(parser)
    parser.add_argument("--stack-name", default="lakitu-{}".format(getpass.getuser()),
                        help="Name that will be assigned to the cluster, the cloudformation stacks and "
                             "resources created. Cannot be the name of an existing stack")
    parser.add_argument("--api-url-prefix", default="lakitu-api",
                        help="A unique prefix that will be used to make a URL for the lakitu REST API, where the final"
                             " URL will be of the form {prefix}.{awsregion}.elasticbeanstalk.com. This prefix must"
                             " not already be in use.",
                        type=cname_prefix_arg)
    parser.add_argument('--no-install', default=False, action='store_true',
                        help="If enabled, this cluster won't be installed to lakitu on this machine. It will "
                             "just be deployed on AWS")
    parser.add_argument('--master-cloudform',
                        default="https://s3.amazonaws.com/atkeller.cloudformation/cloudformation_templates/lakitu/master.yaml",
                        help="Location on s3 of master CloudFormation script for Lakitu infrastructure")
    parser.add_argument('--use-existing-image', default=False, action='store_true',
                        help="If a Lakitu optimized AMI with the specified storage size and type already exists, "
                             "skip image creation and use the existing one. If this flag is not set, a new image is "
                             "created.")
    parser.add_argument("--storage-size", default=1024,
                        help="Size of storage to attach to each cluster instance in GiB)")
    parser.add_argument('--storage-type', default='gp2',
                        help="EBS storage type to attach to each cluster instance")
    parser.add_argument('--container-size', default=200,
                        help="EBS disk space to allocate to each docker container. Multiple containers can be allocated"
                             " to a single EC2 instance, which has one attached EBS volume. This shouldn't be higher"
                             " than the total EBS storage size divided by the maximum expected number of containers per"
                             " instance. Note that the maximum number of containers per instance can be decreased by"
                             " limiting ECS/Batch to smaller EC2 instance types.")
    parser.add_argument('--linux-min-cores', default=0, type=int,
                        help="The minimum number of cores to allow the linux cluster to scale down to")
    parser.add_argument('--linux-max-cores', default=512, type=int,
                        help="The maximum number of cores to allow the linux cluster to scale up to")
    parser.add_argument('--linux-start-cores', default=16, type=int,
                        help="The number of cores to start the cluster with")
    parser.add_argument('--windows-min-instances', default=0, type=int,
                        help="The minimum number of instances to allow the windows cluster to scale down to")
    parser.add_argument('--windows-max-instances', default=128, type=int,
                        help="The maximum number of instances to allow the windows cluster to scale up to")
    parser.add_argument('--windows-start-instances', default=4, type=int,
                        help="The number of instances to start the windows cluster with (must be greater than 0)")
    parser.add_argument('--windows-instance-type', default="c4.xlarge",
                        help="The EC2 instance type to use for the Windows cluster")

    parser.add_argument("--s3-data-bucket", default="maccosslab-lakitudata",
                        help="The S3 databucket (must already exist) to be used by the lakitu cluster for storage."
                             " Most likely, you need to change this parameter to an s3 bucket you have access to")
    parser.add_argument('--disable-rollback', default=False, action="store_true",
                        help="Use this option to disable deletion of AWS resources if they fail to create. Useful"
                             " for development.")
    parser.add_argument("--region-name", default="us-east-1",
                        help="Region to build AWS infrastructure in. Note that some regions may not support all "
                             "features necessary for running Lakitu. It is recommended to use the default.")
    parser.add_argument("--key-name", default="",
                        help="Key Pair name that can be used to SSH/RDP into instances for debugging.")
    parser.add_argument("--extent", default="all", help="[all,cluster,server] deploy one piece of the infrastructure at"
                                                        " a time. Useful for development.")
    return parser


AwsCreds = namedtuple("AwsCreds", ['aws_access_key_id', 'aws_secret_access_key'])


def read_aws_credentials(lakitu_cfg_loc):
    """
    Read the AWS credentials from lakitu configuration
    :param lakitu_cfg_loc: location on the file system of the lakitu configuration
    :return: AwsCreds object with access key id and secret key
    """
    lakitu_config = ConfigParser.RawConfigParser()
    lakitu_config.read(lakitu_cfg_loc)
    aws_creds_loc = lakitu_config.get('aws', 'credentials_file')
    aws_config = ConfigParser.RawConfigParser()
    aws_config.read(aws_creds_loc)
    return AwsCreds(aws_config.get('default', 'aws_access_key_id'),
                    aws_config.get('default', 'aws_secret_access_key'))


def get_cluster_config_path(lakitu_cfg_loc):
    lakitu_config = ConfigParser.RawConfigParser()
    lakitu_config.read(lakitu_cfg_loc)
    return lakitu_config.get('aws', 'clusters_cfg')


def wrap_ecs_image(template_ami, name_prefix='lakitu', ec2_client=None, ec2_r=None,
                   storage_size=1024, storage_type='gp2', container_size=200, use_existing=False, key_name=""):
    """
    Create an AMI optimized for the Lakitu pipeline from an existing Linux ECS instance.
    The wrapped AMI has the specified storage attached and
    Recommended to use one of the base ECS AMIs from Amazon:
    http://docs.aws.amazon.com/AmazonECS/latest/developerguide/ecs-optimized_AMI.html)
    :param template_ami: ECS ami to start with
    :param name_prefix: prefix to name the instance by, instances will be named based on the prefix
                        and specified parameters, for example: lakitu_250_gp2_ami-28456852
    :param ec2_client: an ec2 client to use for example boto3.client('ec2')
    :param ec2_r: an ec2 resource to use
    :param storage_size: size of storage volume for new AMI
    :param storage_type: type of storage volume (https://aws.amazon.com/ebs/details/)
    :param container_size: size available to each docker container (see dm.basesize section at https://docs.docker.com/engine/reference/commandline/dockerd/#options-per-storage-driver)
    :param use_existing: if an existing AMI is found, use it
    :param key_name: KeyPair for accessing the image for debugging
    :return: ID of newly created AMI
    """
    if ec2_client is None:
        ec2_client = boto3.client('ec2')
    if ec2_r is None:
        ec2_r = boto3.resource('ec2')

    image_name = "{}_{}_{}_{}".format(name_prefix, storage_size, storage_type, template_ami)
    # check if image exists
    im_search = ec2_client.describe_images(
        Filters=[
            {'Name': 'name', 'Values': [image_name]},
            {'Name': 'is-public', 'Values': ['false']}])['Images']
    if len(im_search) > 0:
        logger.info("Image with name {} has already been created. AMI is: {}".format(image_name,
                                                                                     im_search[0]['ImageId']))
        if use_existing:
            logger.info("Using existing image with name {}".format(image_name))
            return im_search[0]['ImageId']
        else:
            image_name += "_{}".format(''.join(random.sample(string.ascii_lowercase, 8)))
            logger.info("Creating a new image with name {}".format(image_name))

    logger.info("Deploying template instance for image creation")

    optional_kwargs = {}
    if key_name:
        optional_kwargs['KeyName'] = key_name
    instance = ec2_r.create_instances(
        ImageId=template_ami,
        MinCount=1,
        MaxCount=1,
        InstanceType='t2.micro',
        BlockDeviceMappings=[
            {
                "DeviceName": "/dev/xvdcz",
                "Ebs":
                    {"DeleteOnTermination": True,
                     "VolumeSize": storage_size,
                     "VolumeType": storage_type}
            }
        ],
        UserData=instance_user_data.format(LAKITU_STORAGE_SIZE=container_size),
        **optional_kwargs
    )[0]
    logger.info("Deployed instance id: {}".format(instance.id))
    logger.info("Waiting for deployed instance to initialize")
    instance.wait_until_running()
    logger.info("Instance is in running state")
    logger.info("HACK: Waiting to allow time for install script to run. It would be better to make a call to verify "
                "that the SSM agent is running. This would require modifying the security group used at the time of "
                "creating the instance be opened to allow for networking outside of the default VPC")
    time.sleep(60)
    logger.info("Creating image")

    image = instance.create_image(
        Description="A customized Lakitu ECS image",
        Name=image_name)
    image.wait_until_exists()
    logger.info("Creating new image named {} with AMI ID {}".format(image_name, image.image_id))
    logger.info("Waiting for image creation to complete")
    waiter = ec2_client.get_waiter('image_available')
    waiter.config.delay = 5
    waiter.config.max_attempts = 120
    image_success = False
    try:
        waiter.wait(ImageIds=[image.image_id])
    except WaiterError as we:
        logger.error("It looks like image creation failed: ")
        logger.error(we)
    else:
        logger.info("Image creation successful")
        image_success = True

    logger.info("Terminating template EC2 instance")
    instance.terminate()
    instance.wait_until_terminated()
    logger.info("Instance successfully terminated.")

    if image_success:
        logger.info("Great success!")
    else:
        logger.info("Image creation failed. The EC2 instance the image was created from was terminated, but"
                    " there may be some Snapshots leftover. To find them, in AWS console, go to the EC2 service "
                    "and browse to Elastic Block Store -> Snapshots to see if there are any hanging around")
    return image.image_id


@contextlib.contextmanager
def cd(path):
    CWD = os.getcwd()
    os.chdir(path)
    try:
        yield
    finally:
        os.chdir(CWD)


def wait_for_deploy(args, stack_name, cf_client):
    waiter = cf_client.get_waiter('stack_create_complete')
    # wait up to 45 minutes for stack to build
    waiter.config.delay = 5
    waiter.config.max_attempts = 540
    try:
        waiter.wait(StackName=stack_name)
    except WaiterError as we:
        logger.error("It looks like CloudFormation failed: ")
        logger.error(we)
        if args.disable_rollback:
            logger.error("--disable-rollback was specified, so created resources will need to "
                         "be manually removed. This can be done in the AWS console under CloudFormation")
        else:
            logger.error("Cloudformation rollback is enabled, partially created resources will be automatically "
                         "removed by Amazon.")
        raise


@ch.needsconfig
def deploy(args):
    # The health check for the windows autoscaling group fails when DesiredCapacity is set to 0 with the following
    # error:
    #   "Received 0 SUCCESS signal(s) out of 1. Unable to satisfy 100% MinSuccessfulInstancesPercent requirement"
    # TODO We would like to be able to support starting a cluster with zero instances. This will probably require some
    # more customization of our cloudformation templates. If AWS begins supporting windows for Batch, then this will
    # no longer be an issue.

    # set up the AWS clients
    aws_access_key_id, aws_secret_access_key = read_aws_credentials(args.config_file)
    cf_client = boto3.client('cloudformation',
                             region_name=args.region_name,
                             aws_access_key_id=aws_access_key_id,
                             aws_secret_access_key=aws_secret_access_key)
    cloudformation = boto3.resource('cloudformation',
                                    region_name=args.region_name,
                                    aws_access_key_id=aws_access_key_id,
                                    aws_secret_access_key=aws_secret_access_key)

    ec2_client = boto3.client('ec2',
                              region_name=args.region_name,
                              aws_access_key_id=aws_access_key_id,
                              aws_secret_access_key=aws_secret_access_key)

    ec2 = boto3.resource('ec2', region_name=args.region_name,
                         aws_access_key_id=aws_access_key_id,
                         aws_secret_access_key=aws_secret_access_key)

    eb_client = boto3.client('elasticbeanstalk',
                              region_name=args.region_name,
                              aws_access_key_id=aws_access_key_id,
                              aws_secret_access_key=aws_secret_access_key)

    # Validate input args
    if args.extent not in ['all', 'server', 'cluster']:
        raise NotImplementedError("Unknown option passed to --extent: {}".format(args.extent))

    logger.info("Checking if API URL prefix {prefix} is available...".format(prefix=args.api_url_prefix))

    response = eb_client.check_dns_availability(CNAMEPrefix=args.api_url_prefix)
    if response['Available']:
        logger.info("{url} is available!".format(url=response['FullyQualifiedCNAME']))
    else:
        raise ValueError("API URL prefix {prefix} is not available. Try deploying with another name.".format(prefix=args.api_url_prefix))

    if 'cluster' in args.extent or args.extent == 'all':
        deploy_cluster_outputs = deploy_cluster(args=args, cf_client=cf_client, ec2=ec2, ec2_client=ec2_client)
        stack_arn = deploy_cluster_outputs['stack_arn']

        logger.info("Waiting for stack deployment to complete (this takes ~30 minutes)")

        wait_for_deploy(args, stack_name=args.stack_name, cf_client=cf_client)

        logger.info("Cloudformation stack formation successful for cluster!")
        cf_stack = cloudformation.Stack(stack_arn)
        cf_stack_outputs = {output['OutputKey']: output['OutputValue'] for output in cf_stack.outputs}

        lakituapi_config = PipelineExecutorConfig.from_dict(
            {
                'region': args.region_name,
                'log_group_name': cf_stack_outputs['LogGroup'],
                'log_group_region': cf_stack_outputs['LogRegion'],
                'windows_cluster_name': cf_stack_outputs['WinCluster'],
                'windows_autoscale_name': cf_stack_outputs['WinAutoScalingGroup'],
                'task_arn': cf_stack_outputs['ECSTaskRoleARN'],
                's3_run_bucket': args.s3_data_bucket,
                'linux_job_queue_arn': cf_stack_outputs['LinuxJobQueueArn'],
                'stack_name': args.stack_name,
            }
        )

        logger.info("=========================")
        logger.info("Params for cluster config:")
        logger.info("=========================")
        config_logger = ConfigLogger(logger)
        config_logger(lakituapi_config)

        if not args.no_install:
            logger.info("Installing cluster to your lakitu environment")
            with open(get_cluster_config_path(args.config_file), 'wb') as f:
                lakituapi_config.write(f)
        else:
            logger.info("Cluster installation was disabled, this cluster will not be used by your lakitu install unless"
                        " you run lakitu cluster import!")

        with NamedTemporaryFile(mode='w', delete=False, prefix='cluster', suffix='.cfg', dir=os.getcwd()) as f:
            logger.info("Writing cluster config to {}".format(f.name))
            lakituapi_config.write(f)

    if 'server' in args.extent or args.extent == 'all':
        lakituapi_config = ConfigParser.SafeConfigParser()
        lakituapi_config.read(get_cluster_config_path(args.config_file))

        tempdir = tempfile.mkdtemp()
        try:
            with cd(tempdir):
                response = deploy_lakitu_server(args, cf_client=cf_client, lakituapi_config=lakituapi_config)
        finally:
            shutil.rmtree(tempdir)

        logger.info("Waiting for webserver deployment to complete (this takes ~10 minutes)")

        # HACK: the actual server stack name is duplicated here from the deploy_lakitu_server command.
        # TODO make this non-duplicated information
        wait_for_deploy(args, stack_name="{}-server".format(args.stack_name), cf_client=cf_client)
        logger.info("Cloudformation stack formation successful for lakitu server!")
    logger.info("All done!")


def deploy_cluster(args, cf_client, ec2, ec2_client):
    if args.windows_start_instances <= 0:
        logger.warn("The number of initial instances for the windows cluster should be at least 1 in order to pass"
                    " health checks")

    # build custom AMIs for the Linux batch cluster
    linux_ami = wrap_ecs_image(
        template_ami=get_optimized_ecs_ami_for_region(aws_region=args.region_name, os='linux'),
        ec2_client=ec2_client,
        ec2_r=ec2,
        storage_size=args.storage_size,
        storage_type=args.storage_type,
        container_size=args.container_size,
        use_existing=args.use_existing_image,
        key_name=args.key_name)

    windows_ami = get_optimized_ecs_ami_for_region(aws_region=args.region_name, os='windows')

    # build the batch stack
    logger.info("Building resources in region: {}".format(args.region_name))
    logger.info("Creating CloudFormation stack {}".format(args.stack_name))
    params = {'DataBucketName': args.s3_data_bucket,
              'LinuxMinCpus': str(args.linux_min_cores),
              'LinuxMaxCpus': str(args.linux_max_cores),
              'LinuxStartCpus': str(args.linux_start_cores),
              'LinuxClusterAmi': linux_ami,
              'WindowsInstanceType': args.windows_instance_type,
              'WindowsClusterAmi': windows_ami,
              'WindowsMinInstances': str(args.windows_min_instances),
              'WindowsMaxInstances': str(args.windows_max_instances),
              'WindowsStartInstances': str(args.windows_start_instances)}
    if args.key_name != "":
        params['KeyName'] = args.key_name
    param_cf = [{'ParameterKey': k, 'ParameterValue': v} for k, v in params.iteritems()]
    response = cf_client.create_stack(StackName=args.stack_name,
                                      TemplateURL=args.master_cloudform,
                                      Parameters=param_cf,
                                      Capabilities=['CAPABILITY_NAMED_IAM'],
                                      DisableRollback=args.disable_rollback
                                      )
    stack_arn = response['StackId']
    logger.info("Cloudformation stack ARN: {}".format(stack_arn))
    return {"stack_arn": stack_arn}


def deploy_lakitu_server(args, cf_client, lakituapi_config):
    """
    Deploy elasticbeanstalk instance of lakitu in VPC. This function leaves behind intermediate files so it should
    be used in a disposable temporary directory.
    """

    # Download source of lakitu server dockerapp
    logger.info("Downloading lakituserver source...")
    response = requests.get("https://bitbucket.org/maccosslab/docker-lakituserver/get/v0.3.1.zip")
    with open("dockerapps.zip", 'wb') as f:
        for chunk in response.iter_content(chunk_size=1024):
            if chunk:
                f.write(chunk)
    logger.info("...done!")

    with ZipFile("dockerapps.zip", 'r') as f:
        f.extractall()

    # Write in config file params
    # TODO Put this git hash into a constant to make it clear that it needs to be updated...
    with open('maccosslab-docker-lakituserver-6d47bbdacf48/lakitu_config_files/clusters.cfg', 'w+') as f:
        lakituapi_config.write(f)

    # Zip and upload to s3
    lakituserver_zip_name = '{}-lakituserver'.format(args.stack_name)
    lakituserver_s3_key = lakituserver_zip_name + '.zip'
    shutil.make_archive(lakituserver_zip_name, 'zip', 'maccosslab-docker-lakituserver-6d47bbdacf48')
    s3 = boto3.resource('s3')
    s3.meta.client.upload_file(lakituserver_zip_name + '.zip', lakituapi_config.get('aws', 's3_run_bucket'), lakituserver_s3_key)

    # Deploy using cloudformation template
    params = {
        'ClusterStackName': args.stack_name,
        'SourceZipS3Bucket': args.s3_data_bucket,
        'SourceZipS3Key': lakituserver_s3_key,
        'ApiUrlPrefix': args.api_url_prefix,
    }
    param_cf = [{'ParameterKey': k, 'ParameterValue': v} for k, v in params.iteritems()]
    response = cf_client.create_stack(
        StackName='{}-server'.format(args.stack_name),
        TemplateURL=posixpath.join(posixpath.dirname(args.master_cloudform), 'consumer-apps-master.yaml'),
        Parameters=param_cf,
        Capabilities=['CAPABILITY_NAMED_IAM'],
        DisableRollback=args.disable_rollback
    )

    stack_arn = response['StackId']
    logger.info("Cloudformation webserver stack ARN: {}".format(stack_arn))

    return {
        'stack_arn': stack_arn
    }
