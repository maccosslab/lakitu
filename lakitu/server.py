import ConfigParser
import itertools
import logging
import os
import subprocess

from flask import Flask, abort
from flask_cors import CORS
from lakituapi.pipeline_executor import PipelineExecutor

from lakitu.cli.cli_helpers import find_config_location
from lakitu.pipeline.pipeline_repo import PipelineRepository
from lakitu.util.run_status_manager import RunStatusManager

logger = logging.getLogger(__name__)

app_ref = None

REPO_INDEX_YML = 'repo_index_yml'
CONDA_LOCAL_PIPELINE_DIRECTORY = 'conda_local_pipeline_directory'
DEVNULL = open(os.devnull, 'w')


CONFIG = {
    'repo_index_yml': "https://s3.amazonaws.com/atkeller.lakitu.public/pipelines/pipelines.yaml",
    'conda_local_pipeline_directory': '~/.lakitu/envs'
}

LUIGI_PARAMETER = 'LUIGI_PARAMETER'
LUIGI_HELP = 'LUIGI_HELP'
run_status_manager = RunStatusManager()


# initialization
def extra_init(app):
    # extensions
    cors = CORS(app.app, resources={r"/v1/*": {"origins": "*"}})


def parse_luigi_params_from_help(help_message):
    params = []
    exclusions = ["--local-scheduler",
                  "--module",
                  "--help",
                  "--help-all"]
    for i, line in enumerate(help_message):
        line = line.strip()
        if line.startswith("--"):
            if line.startswith("--MainTask"):
                continue
            param = line.split(" ")[0]
            if not param in exclusions:
                entry = {}
                entry[LUIGI_PARAMETER] = param

                # Get help message text
                help_lines = []
                help_lines += line.split()[2:]  # Remove first two words
                help_lines += list(itertools.takewhile(lambda x: not x.strip().startswith('--'), help_message[i + 1:]))

                entry[LUIGI_HELP] = " ".join([line.strip() for line in help_lines])
                params.append(entry)
    return params


def get_config():
    config = ConfigParser.RawConfigParser()
    config_location = find_config_location()
    if config_location is None:
        logger.warn("Could not find lakitu config file")
        return None
    config.read(config_location)
    config.read(config.get('aws', 'clusters_cfg'))
    return config


def list_pipelines():
    pr = PipelineRepository(repo_index_yml=CONFIG[REPO_INDEX_YML])
    return pr.list_pipelines_and_versions()
    

def validate_pipeline(pipeline_name, pipeline_version):

    if pipeline_name is None or pipeline_version is None:
        abort(400)  # Missing arguments

    # Check if pipeline is valid
    pr = PipelineRepository(repo_index_yml=CONFIG[REPO_INDEX_YML])

    pipelines = pr.list_pipelines_and_versions()

    try:
        versions = pipelines[pipeline_name]

    except KeyError:
        abort(400)  # Pipeline does not exist

    if pipeline_version not in versions:
        abort(400)  # Version does not exist


def run_pipeline(pipeline_name, pipeline_version, parameters):

    validate_pipeline(pipeline_name, pipeline_version)

    # TValidate pipeline args by ensuring that all args passed in are ones that are returned by pipeline_parameters
    available_parameters = pipeline_parameters(pipeline_name, pipeline_version)
    available_param_names = set(entry[LUIGI_PARAMETER] for entry in available_parameters)
    param_names = set(entry[LUIGI_PARAMETER] for entry in parameters)
    if param_names not in available_param_names:
        abort(400)  # Unknown pipeline parameters provided

    # Run pipeline
    pr = PipelineRepository(repo_index_yml=CONFIG[REPO_INDEX_YML])
    pipeline = pr.get_pipeline(pipeline_name=pipeline_name, version=pipeline_version)
    if pipeline.source_type == "conda":
        return run_conda_pipeline(pipeline, parameters, return_output=False)
    else:
        abort(400)  # Unknown source type


# HACK
# TODO This is largely duplicated code from run_pipelines.py, put this in its own module for reuse
def run_conda_pipeline(pipeline, pipeline_args=list(), return_output=False):
    pipeline_env_name = "{}.{}".format(pipeline.pipeline_name, pipeline.version)

    # check if lakitu conda envs path exists, create if need to
    conda_envs_dir = os.path.expanduser(CONFIG[CONDA_LOCAL_PIPELINE_DIRECTORY])
    if not os.path.exists(conda_envs_dir):
        try:
            os.makedirs(conda_envs_dir)
        except OSError:
            logger.error("Could not create local conda environments directory {}".format(conda_envs_dir))
            raise

    # check if pipeline directory exists
    pipeline_env_dir = os.path.join(conda_envs_dir, pipeline_env_name)
    if not os.path.exists(pipeline_env_dir):
        # make the environment
        # pull the pipeline from the repository
        # configure channels and paths
        conda_call = ["conda",
                      "create",
                      "-y",
                      "-p", pipeline_env_dir,
                      "-c", pipeline.channel,
                      "-c", "atkeller",  # needed to get lakituapi
                      "-c", "defaults",
                      "-c", "system",
                      "python=2.7",
                      "{}={}".format(pipeline.pipeline_name, pipeline.version)]
        ret_code = subprocess.call(conda_call)
        if ret_code != 0:
            logger.warn("Creation of conda environment returned non-zero exit status")

    # configure the environment
    # my_env = get_pipeline_execution_environment(CONFIG)
    my_env = PipelineExecutor(config=get_config()).execution_environment
    # set an environ
    python_binary = os.path.join(pipeline_env_dir, "bin", "python")
    interface_module = pipeline.pipeline_name + '.interface'

    #start_luigi()
    pipeline_call = [python_binary, "-m", "luigi", "--workers", "1000", "--module", interface_module, "MainTask"]
    pipeline_call.extend(pipeline_args)
    lakitu_proc = subprocess.Popen(pipeline_call, env=my_env, stdout=subprocess.PIPE)
    current_main_task = run_status_manager.get_main_task_id()
    if return_output:
        output, unused_stderr = lakitu_proc.communicate()
        return {'output': output.splitlines()}
    else:
        return run_status_manager.get_main_task_id(current_main_task)


def pipeline_parameters(pipeline_name, pipeline_version):
    validate_pipeline(pipeline_name, pipeline_version)

    # Run pipeline
    pr = PipelineRepository(repo_index_yml=CONFIG[REPO_INDEX_YML])
    pipeline = pr.get_pipeline(pipeline_name=pipeline_name, version=pipeline_version)

    if pipeline.source_type == "conda":
        output = run_conda_pipeline(pipeline, pipeline_args=['--help'], return_output=True)
    else:
        abort(400)  # Unknown source type

    # Get pipeline parameters
    params = parse_luigi_params_from_help(output['output'])
    return [line for line in params]


def check_run(run_id):
    """ Make call to luigi api with run id and return status """
    # TODO
    return run_status_manager.get_latest_run_status(run_id)  # Not implemented


def start_luigi(luigi_config_path=None):
    # if luigid is running, a duplicate instance will not be created
    if luigi_config_path is not None:
        os.environ["LUIGI_CONFIG_PATH"] = luigi_config_path
    p = subprocess.Popen(['luigid'], stdout=DEVNULL, stderr=DEVNULL, shell=True)


def run(address=None, api_port=5002, debug=False):
    app = Flask(__name__)
    lakitu_config = get_config()
    if lakitu_config is not None:
        luigi_config_path = lakitu_config.get('luigi', 'config_location')
    else:
        logger.warn("Could not find luigi configuration")
        luigi_config_path = None
    start_luigi(luigi_config_path=luigi_config_path)
    extra_init(app)
    app.run(host=address, port=api_port, debug=debug)


if __name__ == '__main__':
    run(address='0.0.0.0', debug=True)
