"""
batch.py

This is a modified version of a module originally written by Jake Feala for Outlier Bio.
The original code is Copyright 2017 Outlier Bio, LLC
The original code is here: https://github.com/outlierbio/ob-pipelines/blob/master/ob_pipelines/batch.py (apache 2 license)

Modifications here are Copyright 2017 Jarrett Egertson
"""

import json
import logging
import multiprocessing
import time
from copy import deepcopy

import luigi
from lakituapi.pipeline import LakituAwsConfig

from awstask import AwsTaskBase, hash_def

logger = logging.getLogger(__name__)

try:
    import boto3
    client = boto3.client('batch', region_name=LakituAwsConfig.region)
except ImportError:
    logger.warning('boto3 is not installed. BatchTasks require boto3')

POLL_TIME = 2

JOBDEF_LOCK = multiprocessing.Lock()    # avoid simultaneous job definition registration requests


def _get_job_def_if_exists(job_def_name):
    """
    Get the most recent active job definition for the family if it exists
    :param job_def_name: name of the job definition
    :return: task definition arn if task exists, otherwise none
    """
    next_token = ""
    max_rev = 0
    job_def_arn = None
    while True:
        response = client.describe_job_definitions(jobDefinitionName=job_def_name,
                                                   nextToken=next_token,
                                                   status='ACTIVE')
        for job_def in response['jobDefinitions']:
            if job_def['revision'] > max_rev:
                max_rev = job_def['revision']
                job_def_arn = job_def['jobDefinitionArn']
        if 'nextToken' not in response or response['nextToken'] == "":
            break
        else:
            next_token = response['nextToken']
    return job_def_arn


def _get_or_register_job_definition(l_job_def, force_registration=False):
    """
    Register a Batch job definition if an active version of it does not already exist
    (determined by family name)
    :param l_job_def: job definition dictionary suitable for boto3 batc client register_job_definition function call
    :param force_registration: if True, job will be registered even if it is determined to already exist
    :return: task definition arn
    """

    job_def = deepcopy(l_job_def)
    # the job definition is hashed to keep from submitting a new revision every time a job is run
    job_def['jobDefinitionName'] = l_job_def['jobDefinitionName'] + '_' + hash_def(job_def)

    if force_registration:
        logger.info("Forced registration for job definition {}".format(job_def['jobDefinitionName']))
        job_def_arn = client.register_job_definition(**job_def)['jobDefinitionArn']
    else:
        # returns None if task def doesn't exist already
        job_def_arn = _get_job_def_if_exists(job_def['jobDefinitionName'])

    if job_def_arn is None:
        logger.info("No job definitions for job def {} found, registering new job".format(job_def['jobDefinitionName']))
        job_def_arn = client.register_job_definition(**job_def)['jobDefinitionArn']
    return job_def_arn


def _get_job_status(job_id):
    """
    Retrieve task statuses from ECS API
    Returns list of {SUBMITTED|PENDING|RUNNABLE|STARTING|RUNNING|SUCCEEDED|FAILED} for each id in job_ids
    """
    response = client.describe_jobs(jobs=[job_id])

    # Error checking
    status_code = response['ResponseMetadata']['HTTPStatusCode']
    if status_code != 200:
        msg = 'Job status request received status code {0}:\n{1}'
        raise Exception(msg.format(status_code, response))

    return response['jobs'][0]['status']


def _wait_on_job(job_id):
    """Poll task status until STOPPED"""

    while True:
        status = _get_job_status(job_id)
        if status == 'SUCCEEDED':
            logger.info('Batch job {} SUCCEEDED'.format(job_id))
            return True
        elif status == 'FAILED':
            # Raise and notify if job failed
            data = client.describe_jobs(jobs=[job_id])['jobs']
            raise Exception('Job {} failed: {}'.format(job_id, json.dumps(data, indent=4)))

        time.sleep(POLL_TIME)
        logger.debug('Batch job status for job {0}: {1}'.format(
            job_id, status))


class BatchTask(AwsTaskBase):
    """
    Base class for a AWS Batch Task
    Amazon Batch requires you to register "jobs", which are JSON descriptions
    for how to issue the ``docker run`` command and provision resources.
    This Luigi Task can either run a pre-registered ECS jobDefinition, OR register
    the job on the fly from a Python dict.

    :param job_def_arn: pre-registered task definition ARN (Amazon Resource
        Name)

    :param job_def: dict describing task in jobDefinition JSON format for
        example::
            job_def = {
                'jobDefinitionName': 'hello-world',
                'volumes': [],
                'containerProperties': [
                    {
                        'memory': 1,
                        'image': 'ubuntu',
                        'command': ['/bin/echo', 'hello world']
                    }
                ]
            }
    Ref: http://boto3.readthedocs.io/en/latest/reference/services/batch.html#Batch.Client.register_job_definition
    """

    job_def_arn = luigi.Parameter(default="")
    job_def = luigi.DictParameter(default=None)
    job_queue_arn = luigi.Parameter()   # ARN for the job queue to submit this task to

    def run(self):
        self.run_batch()

    def run_batch(self, param_overrides=None):
        if self.job_def is None and (not self.job_def_arn):
            raise ValueError('Neither job_def nor job_def_arn are assigned')
        if (self.job_def is not None) and self.job_def_arn:
            raise ValueError('Both task_def and task_def_arn are assigned. Must have only one assigned.')
        if not self.job_def_arn:
            # Register the task and get assigned taskDefinition ID (arn)
            with JOBDEF_LOCK:
                self.job_def_arn = _get_or_register_job_definition(self.job_def)

        command_override = self.command(param_overrides=param_overrides)
        container_overrides = {}
        if len(command_override) > 0:
            container_overrides['command'] = command_override

        # set any environment variables requested by the task
        if len(self.environment) > 0:
            container_overrides['environment'] = self.environment

        # TODO: total length of overrides cannot exceed 8192 characters see
        # http://boto3.readthedocs.io/en/latest/reference/services/ecs.html#ECS.Client.run_task

        job_name = self.job_def['jobDefinitionName'] + '_' + hash_def(container_overrides)
        response = client.submit_job(jobName=job_name,
                                     jobQueue=LakituAwsConfig.linux_job_queue_arn,
                                     jobDefinition=self.job_def_arn,
                                     containerOverrides=container_overrides)
        _wait_on_job(response['jobId'])


