"""
awstask.py

Written by Jarrett Egertson, 2018

A base task class for container tasks
"""

import luigi
import luigi.contrib.s3 as s3
import os
import hashlib
import json


def hash_def(task_or_job_def):
    return hashlib.md5(json.dumps(task_or_job_def, sort_keys=True).encode('utf-8')).hexdigest()


class FilePathParameter(luigi.Parameter):
    """
    Reads in a file path from the command line and converts it automatically to an absolute path
    """
    def parse(self, x):
        if x is None:
            return None
        if x.startswith("s3://"):
            return x
        return os.path.abspath(x)


class AwsTaskBase(luigi.Task):
    @property
    def parameters(self):
        """
        Parameters to pass to the command template

        Override to return a dict of key-value pairs to fill in command arguments
        :return:
        """
        return {}

    @property
    def environment(self):
        """
        Environment variables to set in docker container when command is run

        Override to return a list of key-value pairs. dicts of this format:
        {'name': var_name, 'value': var_value}
        see
        http://boto3.readthedocs.io/en/latest/reference/services/ecs.html#ECS.Client.run_task
        :return:
        """
        return []

    @property
    def downloadable_outputs(self):
        """
        :return: a listing of s3 outputs from this task that should be downloaded from s3 to the system that spawned
        this pipeline run
        """
        o = self.output()
        if isinstance(o, s3.S3Target):
            return [o]
        if isinstance(o, list):
            return [t for t in o if isinstance(t, s3.S3Target)]
        if isinstance(o, dict):
            return [t for t in o.values() if isinstance(t, s3.S3Target)]
        return list()

    def command(self, param_overrides=None):
        """
        Command passed to the containers, derived from command_base with
        replacement of parameters (Ref::name_param) with their value derived
        from the parameters property
        Directly corresponds to the `overrides` parameter of runTask API. For
        example::
            [
                {
                    'name': 'myContainer',
                    'command': ['/bin/sleep', '60']
                }
            ]
        """
        def get_param(arg):
            k = arg.split('::')[1]
            if param_overrides is not None:
                for param_name in param_overrides.keys():
                    if param_name not in self.parameters:
                        raise RuntimeWarning("Tried to override a parameter ({})"
                                             "that is not present in task params.".format(param_name))

            if param_overrides is not None and k in param_overrides:
                return param_overrides[k]
            else:
                return self.parameters[k]
        cmd_args = [get_param(arg) if arg.startswith('Ref::') else arg for arg in self.command_base]
        return cmd_args
