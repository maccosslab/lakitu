"""
ecs.py

This is a modified version of the luigi.contrib.ecs module originally written by Jake Feala for Outlier Bio.
The original code is Copyright 2015 Outlier Bio, LLC
The original code is here: https://github.com/spotify/luigi/blob/master/luigi/contrib/ecs.py (apache 2 license)

Modifications here are Copyright 2017 Jarrett Egertson
"""

import logging
import multiprocessing
import time
from copy import deepcopy

import luigi
from botocore.exceptions import ClientError
from lakituapi.pipeline import LakituAwsConfig

from awstask import AwsTaskBase, hash_def

logger = logging.getLogger(__name__)

try:
    import boto3
    client = boto3.client('ecs', region_name=LakituAwsConfig.region)
    asg_c = boto3.client('autoscaling', region_name=LakituAwsConfig.region)
except ImportError:
    logger.warning('boto3 is not installed. ECSTasks require boto3')

POLL_TIME = 2

ASG_LOCK = multiprocessing.Lock()       # avoid simultaneous autoscale-up calls
TASKDEF_LOCK = multiprocessing.Lock()   # avoid simultaneous job definition registration


def _get_task_statuses(task_ids, cluster_name):
    """
    Retrieve task statuses from ECS API
    Returns list of {RUNNING|PENDING|STOPPED} for each id in task_ids
    """
    response = client.describe_tasks(cluster=cluster_name, tasks=task_ids)

    # Error checking
    if response['failures'] != []:
        raise Exception('There were some failures:\n{0}'.format(
            response['failures']))
    status_code = response['ResponseMetadata']['HTTPStatusCode']
    if status_code != 200:
        msg = 'Task status request received status code {0}:\n{1}'
        raise Exception(msg.format(status_code, response))

    return [t['lastStatus'] for t in response['tasks']]


def _track_tasks(task_ids, cluster_name):
    """Poll task status until STOPPED"""
    while True:
        statuses = _get_task_statuses(task_ids=task_ids, cluster_name=cluster_name)
        if all([status == 'STOPPED' for status in statuses]):
            logger.info('ECS tasks {0} STOPPED'.format(','.join(task_ids)))
            break
        time.sleep(POLL_TIME)
        logger.debug('ECS task status for tasks {0}: {1}'.format(
            ','.join(task_ids), status))


def _get_task_def_if_exists(family):
    """
    Get the most recent active task definition for the family if it exists
    :param family: name of the task family
    :return: task definition arn if task exists, otherwise none
    """
    response = client.list_task_definitions(familyPrefix=family,
                                            status='ACTIVE',
                                            sort='DESC',
                                            maxResults=1)

    if len(response['taskDefinitionArns']) == 0:
        return None
    return response['taskDefinitionArns'][0]


def _get_or_register_task_definition(l_task_def, force_registration=False):
    """
    Register a task definition the ECS cluster if an active version of it does not already exist
    (determined by family name)
    :param l_task_def: task definition dictionary suitable for boto3 ecs client register_task_definition function call
    :param force_registration: if True, task will be registered even if it is determined to already exist
    :return: task definition arn
    """

    task_def = deepcopy(l_task_def)
    # the task definition is hashed to keep from submitting a new revision every time a task is run
    task_def['family'] = l_task_def['family'] + '_' + hash_def(l_task_def)

    if force_registration:
        logger.info("Forced registration for task family {}".format(task_def['family']))
        task_def_arn = client.register_task_definition(**task_def)['taskDefinition']['taskDefinitionArn']
    else:
        # returns None if task def doesn't exist already
        task_def_arn = _get_task_def_if_exists(task_def['family'])

    # if there are no task definition with the same family name, this task must be registered
    if task_def_arn is None:
        logger.info("No tasks for family {} found, registering new task".format(task_def['family']))
        task_def_arn = client.register_task_definition(**task_def)['taskDefinition']['taskDefinitionArn']
    return task_def_arn


def _get_asg_desired_capacity(asg_name):
    """
    Get the current desired capacity of an auto-scaling group
    :param asg_name: the name of the autoscaling group
    :return: the desired capacity
    """
    g_params = asg_c.describe_auto_scaling_groups(AutoScalingGroupNames=[asg_name])['AutoScalingGroups']
    if len(g_params) == 0:
        raise Exception("Could not find an autoscaling group named {}".format(asg_name))
    return g_params[0]['DesiredCapacity']


def _get_asg_max_capacity(asg_name):
    """
    Get the max capacity of an auto-scaling group
    :param asg_name: the name of the autoscaling group
    :return: the maximum capacity of the group
    """
    g_params = asg_c.describe_auto_scaling_groups(AutoScalingGroupNames=[asg_name])['AutoScalingGroups']
    if len(g_params) == 0:
        raise Exception("Could not find an autoscaling group named {}".format(asg_name))
    return g_params[0]['MaxSize']


def _set_asg_desired_capacity(asg_name, desired_capacity):
    """
    Set the desired capacity of an auto-scaling group
    :param asg_name: the name of the autoscaling group
    :param desired_capacity: an integer value for the desired capacity
    :return: set point, the requested capacity
    """
    max_capacity = _get_asg_max_capacity(asg_name)
    set_point = max_capacity if desired_capacity > max_capacity else desired_capacity
    asg_c.set_desired_capacity(AutoScalingGroupName=asg_name,
                               DesiredCapacity=set_point)
    logger.info("Set desired capacity of autoscaling group {} to {}".format(asg_name, set_point))
    return set_point


def _get_instance_count_from_ecs_cluster(cluster_name):
    """
    Get the number of registered container instances on the ecs cluster
    :param cluster_name:
    :return:
    """
    c_params = client.describe_clusters(clusters=[cluster_name])['clusters']
    if len(c_params) == 0:
        raise Exception("Could not find an ECS cluster named {}".format(cluster_name))
    return c_params[0]['registeredContainerInstancesCount']


def _get_instance_cpu_and_mem_from_cluster(cluster_name):
    """
    Return the total registered resources for a container instance on the cluster
    :param cluster_name:
    :return: registered cpu, registered memory: None, None if there are no current instances running
    """
    while True:
        arns = client.list_container_instances(cluster=cluster_name, maxResults=1)['containerInstanceArns']
        if len(arns) == 0:
            return None, None
        instance_arn = arns[0]
        instances = client.describe_container_instances(cluster=cluster_name, containerInstances=[instance_arn])['containerInstances']
        if len(instances) == 0:
            # instance has been removed in between list and describe call
            continue
        resources = {entry['name']: entry['integerValue'] for entry in instances[0]['registeredResources']
                     if entry['name'] in ('CPU', 'MEMORY')}
        return resources['CPU'], resources['MEMORY']


def _request_additional_instances(asg_name, num_instances=1):
    """
    Request that an additional instance be added to the cluster
    :param asg_name: name of the autoscaling group
    :param num_instances: number of instances to add
    :return:
    """
    c_des_capacity = _get_asg_desired_capacity(asg_name)
    _set_asg_desired_capacity(asg_name, c_des_capacity + num_instances)
    time.sleep(2)



class ECSTask(AwsTaskBase):
    """
    Base class for an Amazon EC2 Container Service Task
    Amazon ECS requires you to register "tasks", which are JSON descriptions
    for how to issue the ``docker run`` command. This Luigi Task can either
    run a pre-registered ECS taskDefinition, OR register the task on the fly
    from a Python dict.
    :param task_def_arn: pre-registered task definition ARN (Amazon Resource
        Name), of the form::
            arn:aws:ecs:<region>:<user_id>:task-definition/<family>:<tag>
    :param task_def: dict describing task in taskDefinition JSON format, for
        example::
            task_def = {
                'family': 'hello-world',
                'volumes': [],
                'containerDefinitions': [
                    {
                        'memory': 1,
                        'essential': True,
                        'name': 'hello-world',
                        'image': 'ubuntu',
                        'command': ['/bin/echo', 'hello world']
                    }
                ]
            }
    """

    task_def_arn = luigi.Parameter(default="")
    task_def = luigi.DictParameter(default=None)
    cluster_name = luigi.Parameter(default="default")
    asg_name = None # name of autoscaling group associated with ECS cluster
    container_name = luigi.Parameter(default=None)
    command_base = []

    @property
    def ecs_task_ids(self):
        """Expose the ECS task ID"""
        if hasattr(self, '_task_ids'):
            return self._task_ids

    def run(self):
        # override this if you want to add other stuff after run_ecs
        self.run_ecs()

    def run_ecs(self, param_overrides=None):
        if self.task_def is None and (not self.task_def_arn):
            raise ValueError('Neither task_def nor task_def_arn are assigned')
        if (self.task_def is not None) and self.task_def_arn:
            raise ValueError('Both task_def and task_def_arn are assigned. Must have only one assigned.')
        if not self.task_def_arn:
            # Register the task and get assigned taskDefinition ID (arn)
            with TASKDEF_LOCK:
                self.task_def_arn = _get_or_register_task_definition(self.task_def)

        # Submit the task to AWS ECS and get assigned task ID
        # (list containing 1 string)
        command_override = self.command(param_overrides=param_overrides)
        overrides = {}
        if len(command_override) > 0:
            if 'containerOverrides' not in overrides:
                overrides = {'containerOverrides': [{'name': self.container_name}]}
            overrides['containerOverrides'][0]['command'] = command_override

        # set any environment variables requested by the task
        if len(self.environment) > 0:
            if 'containerOverrides' not in overrides:
                overrides = {'containerOverrides': [{'name': self.container_name}]}
            overrides['containerOverrides'][0]['environment'] = self.environment
        # TODO: total length of overrides cannot exceed 8192 characters see
        # http://boto3.readthedocs.io/en/latest/reference/services/ecs.html#ECS.Client.run_task
        self._task_ids = list()
        retry_count = 0
        add_requests = 0    # number of times addition of an instance was requested by this task
        while True:
            response = {}
            try:
                response = client.run_task(cluster=self.cluster_name,
                                           taskDefinition=self.task_def_arn,
                                           overrides=overrides)
            except ClientError as e:
                if "No Container Instances were found in your cluster." in e:
                    # This is expected as the requested instances are coming online
                    pass
                else:
                    logger.error("Received error: %s", e, exc_info=True)
                self._task_ids = []
            if 'tasks' in response:
                self._task_ids = [task['taskArn'] for task in response['tasks']]
            else:
                self._task_ids = []

            if len(self._task_ids) > 0:
                # successful task submission
                break
            # if there's no autoscaling group given, just have to retry the task submission and hope
            if self.asg_name is None and retry_count < 5:
                time.sleep(10)
                retry_count += 1
                continue
            # there is an autoscaling group
            with ASG_LOCK:
                # acquire the lock to adjust autoscaling (avoid concurrent requests)
                inst_count = _get_instance_count_from_ecs_cluster(self.cluster_name)
                desired_count = _get_asg_desired_capacity(self.asg_name)
                if inst_count < desired_count:
                    # the cluster is in teh process of adding instances
                    if add_requests == 0 and desired_count - inst_count < 5:
                        # number of times additional instance was requested
                        add_requests += 1
                        _request_additional_instances(self.asg_name, 1)
                else:
                    # the cluster has the desired number of instances or is downscaling, request more
                    _request_additional_instances(self.asg_name, 1)
                    add_requests += 1
            time.sleep(30)

        if len(self._task_ids) == 0:
            print response
        # Wait on task completion
        _track_tasks(task_ids=self._task_ids, cluster_name=self.cluster_name)

