Cloudformation templates to build lakitu infrastructure on AWS.
If you want to modify the template, you will need to upload all of the .yaml
files to your own s3 locations and edit their locations in master.yaml

push_to_s3.sh demonstrates how one could programmatically update the s3
cloudformation repository but will only work if you have access to the s3 path
in the script

lakitu/scripts/deploy.py demonstrates programmatic creation of the cloudformation
stack using the default template (and its respective default template parameters).
