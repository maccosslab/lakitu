#!/bin/bash

# Clean previous builds. If this step isn't done then the final package sizes increase dramatically.
rm -r ~/anaconda2/conda-bld/linux-64 linux-32 win-64 win-32 osx-64

# output stdout and stderr to file and to console
conda build conda.recipe --channel=atkeller |& tee build.log
if [ ${PIPESTATUS[0]} -ne 0 ]; then
    echo ERROR: Failed to build. See build.log
    exit 1
fi

build_path=`grep "anaconda upload" build.log | cut -d " " -f 3 | tail -n 1`
build_name=`basename $build_path`

conda convert -p all $build_path
anaconda upload --force $build_path
anaconda upload --force linux-32/$build_name
anaconda upload --force win-64/$build_name
anaconda upload --force win-32/$build_name
anaconda upload --force osx-64/$build_name
